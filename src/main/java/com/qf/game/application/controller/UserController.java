package com.qf.game.application.controller;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.qf.game.application.entity.User;
import com.qf.game.application.service.IUserService;
import com.qf.game.application.utils.MapCodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Controller -> Service -> Dao
 */
@RestController
@RequestMapping("/user")
//解决跨域问题 ?????
@CrossOrigin
public class UserController {

    @Autowired
    private IUserService userService;

    @Autowired
    private DefaultKaptcha kaptcha;

    /**
     * 用来暂存验证码的map集合
     */
    private Map<String, String> codeMap = new HashMap<>();

    /**
     * 用户注册
     * @return
     */
    @RequestMapping("/register")
    public int register(User user){

        //调用用户的业务层
        // -1 - 当前用户名已经存在
        // -2 - 当前邮箱已经存在
        // 1 - 注册成功
        int result = userService.register(user);
        return result;
    }

    /**
     * 登录的功能
     * @return
     */
    @RequestMapping("/login")
    public User login(String username, String password, String code, String codekey){
        //获取服务生成的验证码
        String serverCode = codeMap.get(codekey);
        if (!code.equals(serverCode)) {
            User user = new User();
            user.setUsername("-1");
            return user;
        }

        //验证码匹配正常，删除map中保存的验证码
        codeMap.remove(codekey);

        //user不为null，说明用户登录成功
        //user为null，说明用户名或者密码错误
        User user = userService.login(username, password);
        return user;
    }

    /**
     * 生成验证码
     */
    @RequestMapping("/code")
    public void createCode(String codekey, HttpServletResponse response) throws IOException {
        //生成验证码
        String code = kaptcha.createText();

        //保存到Map集合中
        codeMap.put(codekey, code);

        //生成图片
        BufferedImage image = kaptcha.createImage(code);
        //直接将图片 通过response对象响应给客户端
        ImageIO.write(image, "jpg", response.getOutputStream());
    }


    /**
     * 通过账号 给邮箱发送 找回密码的 验证码
     * @return
     */
    @RequestMapping("/sendEmailCode")
    public int sendEmailCode(String username){
        int result = userService.sendEmailByUserName(username);
        return result;
    }

    /**
     * 找回密码
     * @return
     */
    @RequestMapping("/updatePassword")
    public int updatePassword(String username, String newpassword, String code){

        //校验验证码 用户输入的验证码 - 发邮件的验证码 （一次性 时效性-5分钟）
        //获取原来发邮件的验证码
        String oldCode = MapCodeUtils.getCode(username);
        if (oldCode == null) {
            //无效或已过期的验证码
            return -1;
        }

        //比较验证码
        if (!oldCode.equals(code)) {
            //验证码有误
            return -2;
        }

        //验证码已经通过
        //调用service层 修改密码
        int result = userService.updatePassword(username, newpassword);

        //将验证码删除
        MapCodeUtils.removeCode(username);
        return result;
    }
}
