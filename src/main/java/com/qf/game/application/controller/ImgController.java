package com.qf.game.application.controller;

import com.qf.game.application.service.IUserService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.UUID;

/**
 * 新写的注释 ------ ！！！！！ 1231312 qqwwee
 * 12313212312313
 * aaaaaaaaa
 * bbbbbbbbbbbbbb
 */
@RestController
@CrossOrigin
public class ImgController {

    @Autowired
    private IUserService userService;

    /**
     * 图片的上传路径
     */
    private String path = "D:\\img\\";

    /**
     * 上传图片
     *
     * @return 返回图片的上传后的名称
     */
    @RequestMapping("/img/uploader")
    public String uploaderImg(String username, MultipartFile file) {

        //基于UUID生成一个文件名称（防止上传的图片重复）
        String fileName = UUID.randomUUID().toString();

        //IO流
        try (
                InputStream in = file.getInputStream();
                OutputStream out = new FileOutputStream(path + fileName);
        ) {
            IOUtils.copy(in, out);

            //最新的头像名称，更新到数据库
            userService.updateHeader(username, fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileName;
    }

    /**
     * 通过图片名称 获取图片的内容
     *
     * @param imgName
     */
    @RequestMapping("/img/getImg")
    public void getImg(String imgName, HttpServletResponse response) {
        //获取本地的图片
        try (
                InputStream in = new FileInputStream(path + imgName);
                OutputStream out = response.getOutputStream();
        ) {
            IOUtils.copy(in, out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
