package com.qf.game.application.service;

import com.qf.game.application.entity.User;

public interface IUserService {

    /**
     * 用户注册
     * @param user
     * @return
     */
    int register(User user);

    /**
     * 登录
     * @param username
     * @param password
     * @return
     */
    User login(String username, String password);

    /**
     * 通过用户名 发送找回密码的邮件
     * @param username
     * @return
     */
    int sendEmailByUserName(String username);

    /**
     * 重置密码
     * @return
     */
    int updatePassword(String username, String newPassword);

    /**
     * 根据用户名修改头像
     * @param username
     * @param header
     * @return
     */
    int updateHeader(String username, String header);

    /**
     * 根据用户id 新增欢乐豆
     * @param uid
     * @param happyBean
     * @return
     */
    int addHappyBean(Integer uid, Integer happyBean);
}
