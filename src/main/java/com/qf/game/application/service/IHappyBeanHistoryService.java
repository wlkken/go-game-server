package com.qf.game.application.service;

import com.qf.game.application.entity.HappyBeanHistory;

import java.util.List;

/**
 * 欢乐豆流水记录 业务层
 */
public interface IHappyBeanHistoryService {

    int insertHistory(HappyBeanHistory happyBeanHistory);

    /**
     * 要分页
     * @param uid
     * @return
     */
    List<HappyBeanHistory> queryHistoryByUid(Integer uid);
}
