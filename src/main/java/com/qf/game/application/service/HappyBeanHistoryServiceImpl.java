package com.qf.game.application.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qf.game.application.dao.HappyBeanHistoryDao;
import com.qf.game.application.entity.HappyBeanHistory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 欢乐豆流水记录 业务实现层
 */
@Service
public class HappyBeanHistoryServiceImpl implements IHappyBeanHistoryService {

    @Autowired
    private HappyBeanHistoryDao happyBeanHistoryDao;

    /**
     * 新增欢乐豆流水记录
     * @param happyBeanHistory
     * @return
     */
    @Override
    public int insertHistory(HappyBeanHistory happyBeanHistory) {
        return happyBeanHistoryDao.insert(happyBeanHistory);
    }

    /**
     * 根据用户id查询欢乐豆流水记录列表
     * @param uid
     * @return
     */
    @Override
    public List<HappyBeanHistory> queryHistoryByUid(Integer uid) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("u_id", uid);
        return happyBeanHistoryDao.selectList(queryWrapper);
    }
}
