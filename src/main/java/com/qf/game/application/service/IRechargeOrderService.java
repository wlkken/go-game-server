package com.qf.game.application.service;

import com.qf.game.application.entity.RechargeOrder;

import java.util.List;

/**
 * 充值订单表
 */
public interface IRechargeOrderService {

    int insertOrder(RechargeOrder rechargeOrder);

    List<RechargeOrder> queryRechargeOrderByUid(Integer uid);

    int updateOrderStatus(String orderId, Integer status);

    RechargeOrder getByOrderId(String orderId);

    String alipayOrder(String orderId);
}
