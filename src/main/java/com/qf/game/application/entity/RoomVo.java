package com.qf.game.application.entity;

/**
 * 展示到前端的房间对象
 */
public class RoomVo {

    private Integer rid;//房间编号
    private String nickName;//房主的昵称
    private String info;//房间信息
    private int isPass;//是否包含密码 0 - 没有密码 1 - 有密码
    private int status;//房间状态

    public Integer getRid() {
        return rid;
    }

    public void setRid(Integer rid) {
        this.rid = rid;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getIsPass() {
        return isPass;
    }

    public void setIsPass(int isPass) {
        this.isPass = isPass;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
