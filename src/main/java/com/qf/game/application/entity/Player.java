package com.qf.game.application.entity;

import javax.websocket.Session;

/**
 * 玩家类
 */
public class Player {

    private User user;
    private Session session;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }
}
