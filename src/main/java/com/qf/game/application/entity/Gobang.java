package com.qf.game.application.entity;

/**
 * 棋盘类 - 存放所有玩家的落子数据，以及判断输赢
 */
public class Gobang {

    //使用二维数组代表棋盘
    private Integer[][] gobang = new Integer[17][17];

    //落子的方法 返回true 表示可以落子 返回false 表示不能落子
    public boolean go(int x, int y, int color){
        Integer z = gobang[x][y];
        if (z == null) {
            //说明当前位置为null，可以落子
            gobang[x][y] = color;
            return true;
        }
        return false;
    }

    //根据落子的坐标 判断是否胜利 返回true表示胜利
    public boolean isWin(int x, int y, int color) {

        //记录原始的x 和 y的坐标
        int clientX = x;
        int clientY = y;

        //判断横向 - 左边
        int count = 1;
        while(x > 0 && gobang[--x][y] != null && gobang[x][y] == color){
            count++;
        }
        x = clientX;
        y = clientY;
        //右边
        while(x < gobang.length - 1  && gobang[++x][y] != null && gobang[x][y] == color){
            count++;
        }
        x = clientX;
        y = clientY;
        if (count >=5) {
            //胜利
            return true;
        }

        //判断纵向
        count = 1;
        //向上判断
        while(y > 0 && gobang[x][--y] != null && gobang[x][y] == color){
            count++;
        }
        x = clientX;
        y = clientY;
        //向下判断
        while(y < gobang.length - 1 && gobang[x][++y] != null && gobang[x][y] == color){
            count++;
        }
        x = clientX;
        y = clientY;
        if (count >= 5) {
            return true;
        }

        //左斜 \
        count = 1;
        //左上方判断
        while(x > 0 && y > 0  && gobang[--x][--y] != null && gobang[x][y] == color){
            count++;
        }
        x = clientX;
        y = clientY;
        //右下方
        while(x < gobang.length - 1 && y < gobang.length - 1 && gobang[++x][++y] != null && gobang[x][y] == color){
            count++;
        }
        x = clientX;
        y = clientY;
        if (count >= 5) {
            return true;
        }

        //右斜 /
        count = 1;
        //右上方
        while(x < gobang.length - 1 && y > 0 &&  gobang[++x][--y] != null && gobang[x][y] == color){
            count++;
        }
        x = clientX;
        y = clientY;
        //左下方
        while(x > 0 && y < gobang.length - 1 && gobang[--x][++y] != null && gobang[x][y] == color){
            count++;
        }
        x = clientX;
        y = clientY;
        if (count >= 5) {
            return true;
        }

        return false;
    }
}
